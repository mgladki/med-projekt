#ifndef BFR_CPP
#define BFR_CPP

#include "Data.cpp";
#include "Vector.cpp";
#include "Resolver.cpp";
#include <vector>

/*
 *	author: Gregdeg
 */
class BruteForceResolver: public Resolver{

public:
	BruteForceResolver(Data * data){
		this->data = data;
	}

	std::vector< Vector *> findSimilar(Vector* vector){
		std::cout << "Searching for similar vectors using Brute Force method\n";
		std::cout << "\t[Not Yet Implemented]\n";
		return candidates; //TODO fill candidates
	}

};
#endif