#include <iostream>
#include <fstream>

#include "Data.cpp";

using namespace std;

/*
 *	author: Maciej Gladki
 */
class FileLoader {
	
private:
	Data * data;

public:
	static bool needToConvertToZPN;

	FileLoader(Data * data){
		this->data = data;
	}

	/* zaladuj dane z pliku */
	void loadDataFromFile(string filename){

		/* niezbedne zmienne */
		int numberOfVectors;
		string val;
		string readData;
		fstream plik;
		bool debug = false;

		/* otworzenie pliku */
		cout << "Opening file " << filename << std::endl;
		plik.open(filename);

		/* pobranie informacji o strukturze pliku (liczba wektorow) */
		getline(plik,readData);
		stringstream tmp(readData);
		getline(tmp, val, ' '); // liczba wektorow
		istringstream iss(val);
		iss >> numberOfVectors;

		/* odczyt wektorow */
		cout << "Reading " << numberOfVectors << " vectors... " << std::endl;
		for(int i =0 ; i< numberOfVectors; i++){
			if(i%1000 == 0 && i != 0) cout << i << " vectors done, still reading...\n";
			std::stringstream vectorIdentifier;
			vectorIdentifier << "v" << (i+1);
			Vector *vector = new Vector(vectorIdentifier.str());
			getline(plik,readData);
			stringstream tmp(readData);
			int v = 0;
			int key, value;
			while(getline(tmp, val, ' ') ){
				istringstream iss(val);
				if(v == 0){
					iss >> key;
				}
				else {
					iss >> value;

					// konwersja w razie potrzeby
					if(needToConvertToZPN)
					{
						if(value ==1)
							value=-1;
						else if(value>1)
							value=1;
						else if(value<-1)
							value=-1;
					}
					
					vector->addValue(key,value);
					if(debug) cout << "(" << key << "," << value << ") ";
				}
				v = (v + 1) %2;
			}
			if(debug) cout << "\n";
			data->addVector(vector);
		}
	   cout << "Reading " << numberOfVectors << " vectors finished" << std::endl;
		/* zamkniecie pliku */
       plik.close();
	}
};