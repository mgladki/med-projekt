﻿#ifndef RESOLVER_CPP
#define RESOLVER_CPP

#include <fstream>
#include "Data.cpp";
#include "Vector.cpp"
#include <vector>
#include <ctime>

class Resolver{

protected:
	Data * data;

public:
	static double epsilon;
	bool ifCosine;
	bool bin;
	std::vector< Vector *>  candidates;
	std::vector<std::vector< Vector *> > candidatesAll;
	float timeElapsed;
	static std::fstream theFile;

	bool isResultAvailable;

	int time;
	int curr;


	Resolver(){
		isResultAvailable = false;
		time = -1;
		curr = -1;
	}

	virtual std::vector< Vector *> findSimilar(Vector* vector) = 0;

	void printResult(Data * dataWzor){
		int l=dataWzor->setOfVectors.size();
		/* jesli nie bylo wczesniej obliczone */
		if( !this->isResultAvailable){
			clock_t start, stop;
			candidatesAll.clear();
			start=clock();
			for (int k=0;k<l;k++){
				curr = k;
			candidates  = findSimilar(dataWzor->setOfVectors[k]);
			candidatesAll.push_back(candidates);
			}
			stop=clock();
			timeElapsed=(float)(stop-start)/CLK_TCK;
			time = timeElapsed;
			isResultAvailable = true;
		}

		for (int j=0;j<candidatesAll.size();j++){
			candidatesAll[j];
		if(candidatesAll[j].size() >0){
			std::cout << "\t[Result ready] Number of candidates: " << candidatesAll[j].size() << "\n";
			if(candidatesAll[j].size() <= 10){
				std::cout << "\tCandidates are:\n\t";
				for(std::vector<Vector*>::iterator it = candidatesAll[j].begin(); it != candidatesAll[j].end(); it++) {
					Vector *currentVector = *it;
					std::cout << " " << currentVector->getIdentifier();
				}
			std::cout<<"\n";
			}

			/*	if(this->candidates.size() >0){
			std::cout << "\t[Result ready] Number of candidates: " << this->candidates.size() << "\n";
			if(this->candidates.size() <= 10){
				std::cout << "\tCandidates are:\n\t";
				for(std::vector<Vector*>::iterator it = candidates.begin(); it != candidates.end(); it++) {
					Vector *currentVector = *it;
					std::cout << " " << currentVector->getIdentifier();
				}*/
		} else{
			std::cout << "\tNo candidates";
		}
		}
		std::cout << std::endl <<std::endl;
		std::cout.setf(std::ios::fixed, std::ios::floatfield);
		std::cout.precision(2);
		std::cout << "Time elapsed: " << timeElapsed<<"s";
		std::cout << std::endl <<std::endl;

		// Zapis do pliku
		theFile.open("zpnOutput.txt",std::ios::app);
		if(!theFile.good())
		{
		std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
		}

		else if(theFile.good())
		{
			std::cout<<"Writing to the file... ";
			theFile<<"Method: ";
			if(ifCosine)
				theFile<<"cosine\n";
			else
				theFile<<"Tanimoto\n";
			theFile << "Epsilon: "<<epsilon<<"\n\n";
			theFile << "Time elapsed:  " << timeElapsed << "\n\n";
			for (int j=0;j<candidatesAll.size();j++){
				if(candidatesAll[j].size() >0){
					theFile << "Number of candidates: " << candidatesAll[j].size() << "\n";
					for(std::vector<Vector*>::iterator it = candidatesAll[j].begin(); it != candidatesAll[j].end(); it++) {
						Vector *currentVector = *it;
						theFile << " " << currentVector->getIdentifier();
					}
					theFile<<"\n";
				}else
				{
					theFile << "No candidates\n\n";
				}
			}
			theFile<<"\n\n";
			std::cout<<"Done\n\n";
		}
		theFile.close();
	}
};
#endif