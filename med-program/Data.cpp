﻿#ifndef DATA_CPP
#define DATA_CPP

#include <vector>
#include <hash_map>
#include "Vector.cpp"
#include <sstream>
#include <iostream>
#include <algorithm>

#include <ctime>

/*
*	author: Maciej Gladki
*/
class Data {

public:

	/* Zbior wektorow w ktorych nastapi przeszukiwanie podobnych wektorow */
	std::vector< Vector *> setOfVectors;

	/* Wektor do ktorego szukamy podobnego */
	Vector * vectorOfInterest;

	/* Odwrocone indeksy */
	std::vector< std::vector<Vector *>> invInd;

	/* Kolejność wymiarów według ilości wystąpień w zbiorze danych */
	std::vector<Pair *> dimPresenceCount;

	/* Dodaj wektor do zbioru wektorow do przeszukiwania */
	void addVector(Vector *vector){
		setOfVectors.push_back(vector);
	}

	/* Ustaw wektor ktorego szukamy */
	void setVectorOfInterest(Vector* vector){
		vectorOfInterest = vector;
	}

	/* Przelicz odwrocone indeksy */
	void calculateInvertedInd(){
		invInd= invertedIndices(setOfVectors);
		orderDimensions();
		std::cout << "done preparing data\n";
	}
	void calculateInvertedInd2(std::vector<Pair *>newdimP ){
		//invInd= invertedIndices(setOfVectors);
		//std::vector<Pair *> newdimP=orderDimensions2();
		sortorderedDimensions(newdimP);
		std::cout << "done preparing data\n";
	}

	/* Posortuj po d³ugoœci */ 
	bool sortWRTLength(){
		if(setOfVectors.size()<1)
			return false;

		std::cout<<"Sorting by length... ";
		clock_t start, stop;
		start=clock();
		std::sort(
			setOfVectors.begin() , //zerowy element jest wzorem
			setOfVectors.end(),
			[](Vector *a, Vector *b) {
				return a->getLength() < b->getLength();   
		}
		);
		stop=clock();
		std::cout<<"Done in "<<(float)(stop-start)/CLK_TCK<<" seconds\n";

		return true;
	}

	std::vector< std::vector<Vector *>> invertedIndices (std::vector< Vector *> setOfValues) {
		clock_t start,stop;
		std::hash_map<int,int>  sets; 
		std::hash_map<int,int>  ::iterator it;
		std::vector< std::vector<Vector *>> odwr;
		Vector * nowy;
		int max_dim=0;
		int ile =0;
		std::vector<Vector *> a;
		//a puste
		odwr.push_back (a); //wymiar zero, tego nikt nie ma
		std::cout << "\nCalculating inverted indices...\n";
		start=clock();
		for (int i=0;i<setOfValues.size(); i++){ //0wy jest wzorem//dla kazdego wektora
			if(i%1000 == 0) std::cout<< i << " vectors processed, still processing...\n";
			sets=setOfValues[i]->getSetOfValues();
			for ( it=sets.begin(); it!=sets.end(); it++){  //dodajemy go w kazdy wers w wymiarem ktory u niego nie jest zerowy
				if (it->second!=0){ //niezerowe

					if (it->first >max_dim){ //trzeba powiekszyc max_dim
						ile =it->first - max_dim;
						max_dim=it->first;
						//  std::cout<< "zzz" <<std::endl;
						for ( int l=0; l<ile;l++){
							//std::cout<<"wrzucam a"  <<std::endl;
							odwr.push_back (a); //wymiar 1 wrzucam
							// odwr.push_back (a); 
						}
					}

					odwr[it->first].push_back( setOfValues[i]);


				}

			}
			//std::cout<<" skonczyl i: "<<i  <<std::endl;
		}
		stop=clock();
		std::cout<<"Done in "<<(float)(stop-start)/CLK_TCK<<" seconds";

		return odwr;
	}

	void orderDimensions(){
		dimPresenceCount.clear();
		int tmp;
		std::cout << "\nCalculating dimensions frequency...\n";
		for(int v = 1; v < invInd.size(); v++){
			if(v%100000 == 0) std::cout<< v << " dimensions calculated, still processing...\n";
			tmp = 0;
			std::vector<Vector *> current = invInd[v];
			for(int i = 0 ; i < current.size(); i++){
				Vector* currentVector = current[i];
				tmp ++;
			}
			dimPresenceCount.push_back(new Pair(v,tmp));
		}
		std::sort(dimPresenceCount.begin(), dimPresenceCount.end(),compareByFrequency);
		std::cout << "\nCalculated following order of dimensions by frequency {";
		for(int v = 1; v < invInd.size(); v++){
			std::cout  << dimPresenceCount[v-1]->dimension << " " ;
			if(v>21) {
				std::cout << "... printing to console aborted due to big number of data ("<< invInd.size() <<" elements)\n";
				break;
			}
		}
		std::cout << "}\n";

		std::hash_map<int, int> newOrder;
		for(int i =0; i < dimPresenceCount.size(); i ++ ){
			newOrder.insert(std::pair<int,int>(dimPresenceCount[i]->dimension ,i));
		}

		double time = 0.0;
		clock_t start, stop;
		std::cout << "****Sorting vectors by frequency of dimensions... ";
		start=clock();
		for (int i=0;i< setOfVectors.size(); i++){
			if((i)%1000 == 0) std::cout<< i << " vectors sorted, still sorting...\n ";
			setOfVectors[i]->sortByFreqOfDim(&newOrder);
			//setOfVectors[i]->setDimPresenceCount(&dimPresenceCount);
		}
		stop=clock();
		time = (double)(stop-start)/CLK_TCK;
		std::cout << "done in " << time << " seconds\n";

	}
std::vector<Pair *>  orderDimensions2(){
		//std::vector<Pair *>  dimP2;
		dimPresenceCount.clear();
		int tmp;
		std::cout << "\nCalculating dimensions frequency...\n";
		for(int v = 1; v < invInd.size(); v++){
			if(v%100000 == 0) std::cout<< v << " dimensions calculated,\t";
			tmp = 0;
			std::vector<Vector *> current = invInd[v];
			for(int i = 0 ; i < current.size(); i++){
				Vector* currentVector = current[i];
				tmp ++;
			}
			dimPresenceCount.push_back(new Pair(v,tmp));
		}
		std::sort(dimPresenceCount.begin(), dimPresenceCount.end(),compareByFrequency);
 return dimPresenceCount;
 //return dimP2;
	}

void sortorderedDimensions(std::vector<Pair *> newdimP){
		//dimPresenceCount =newdimP;

		std::hash_map<int, int> newOrder;
		for(int i =0; i < newdimP.size(); i ++ ){
			newOrder.insert(std::pair<int,int>(newdimP[i]->dimension ,i));
		}

		double time = 0.0;
		clock_t start, stop;
		std::cout << "****Sorting vectors by frequency of dimensions... ";
		start=clock();
		for (int i=0;i< setOfVectors.size(); i++){
			if((i)%100 == 0) std::cout<< i << " done,\n ";
			setOfVectors[i]->sortByFreqOfDim(&newOrder);
			//setOfVectors[i]->setDimPresenceCount(&dimPresenceCount);
		}
		stop=clock();
		time = (double)(stop-start)/CLK_TCK;
		std::cout << "done in " << time << " seconds\n";

	}

	static bool compareByFrequency(const Pair * a, const Pair * b)
	{
		return a->value < b->value;
	}

	std::string print(){
		std::stringstream po;
		if(setOfVectors.size() <= 50){
			po << "Representation of inserted data\n";
			for(int v = 0; v < setOfVectors.size(); v++){
				Vector *currentVector = setOfVectors[v];
				po << currentVector->getIdentifier() << " {"; 
				std::hash_map<int,int> setOfValues = currentVector->getSetOfValues();
				for(std::hash_map<int,int>::iterator iter = setOfValues.begin(); iter != setOfValues.end(); ++iter){
					po << " (" << (iter->first) << ", " << iter->second << ") ";
				}
				po << "} \n\t\t\t\tlength: "<< currentVector->getLength() <<"\n";
			}
		}
		else {
			po << "Data is to huge to print in console. Printing aborted.\n";
		}
		return po.str();
	}

	std::string printInvInd(){
		std::stringstream po;
		if(invInd.size() <= 50){
			po << "Representation of inverted indices\n";
			po << "-------------------------------------\n";
			po << "dim\t|\tI(dim)\n";
			po << "-------------------------------------\n";
			for(int v = 1; v < invInd.size(); v++){
				po << v << "\t|\t";
				std::vector<Vector *> current = invInd[v];
				for(int i = 0 ; i < current.size(); i++){
					Vector* currentVector = current[i];
					po<< currentVector->getIdentifier() << " ";
				}
				po <<  std::endl;
			}
		}
		else {
			po << "Data is to huge to print in console. Printing aborted.\n";
		}
		return po.str();
	}











};
#endif