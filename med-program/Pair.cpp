#ifndef PAIR_CPP
#define PAIR_CPP

/*
 *	author: Maciej Gladki
 */
class Pair {

public: 
	Pair(int dimension, int value){
		this->dimension = dimension;
		this->value = value;
	}

	int dimension;
    int value;
	
	 bool operator<(const Pair& a) const
    {
        return value < a.value;
    }

};

#endif