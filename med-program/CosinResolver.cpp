#ifndef COS_CPP
#define COS_CPP

#include "Data.cpp";
#include "Vector.cpp";
#include "Resolver.cpp";
#include <vector>

/*
 *	author: Gregdeg
 */
class CosinResolver: public Resolver{


public:
	CosinResolver(Data * data){
		this->data = data;
	}

	std::vector< Vector *> findSimilar(Vector* vector){
		std::cout << "Searching for similar vectors using Cosin method\n" ;
		std::vector<Vector*> candidates;
		for(std::vector<Vector*>::size_type i=0;
			i<data->setOfVectors.size();i++){
			if(cosineSimilarity(vector, data->setOfVectors[i])>=epsilon)
				candidates.push_back(data->setOfVectors[i]);
		}

		return candidates; //TODO fill candidates
	}

	static double cosineSimilarity(Vector *v1, Vector *v2){
		return v1->dotProduct(v2)/(v1->getLength()*v2->getLength());
	}
};
#endif
