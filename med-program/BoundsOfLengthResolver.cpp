#ifndef BOL_CPP
#define BOL_CPP
#include "Pair.cpp";
#include "Data.cpp";
#include "Vector.cpp";
#include "Resolver.cpp";
#include <vector>
#include "Constants.cpp";


/*
 *	author: monika w
 */
class BoundsOfLengthResolver: public Resolver{


public:
	BoundsOfLengthResolver(Data * data):Resolver(){
		this->data = data;
	}

		std::vector< Vector *> findSimilar(Vector* vector ){ //int numberOfVectors 
		std::vector< Vector *> result;
	 
		 
		std::cout << "Searching for similar vectors using Bounds of Length method\n";
		//vector=data->vectorOfInterest;
		double l =vector->getLength();
		if(Constants::debug) std::cout<<" dl wzoru "<<l <<std::endl;
		 
		//bool bin=true;
		std::vector< Pair* >::iterator c;
		std::vector< Vector *>:: iterator v;
		 
		 int max_w =0;
		 double min, max;
		 double min2,max2;
		 double e;
		 //wz�r pierwszy
		 e=epsilon;
		 if (ifCosine) {
		 min = e*l;
		 max=l/e;}
		 //wzor drugi
		 if(!ifCosine){
		 min=sqrt(e)*1;
		 max=1/sqrt(e) ;
		 }
		 //std::cout<<"ma2x: " <<max2<< " min2: " <<min2 <<" dl l wzoru" << l<<std::endl;
		 //if(Constants::debug)
			 std::cout<<"max: " <<max<< " min: " <<min  <<std::endl;
		 
		 int il = data->setOfVectors.size(); //ilosc wektorow
		  
		 v=data->setOfVectors.begin(); 
		 Vector * a ;
		 
		 if (!bin){  
			 if(Constants::debug)std::cout<<"szukanie po wszystkich"<<std::endl;
			 for (int k=0; k<il;k++)
			 {
				 a=data->setOfVectors[k];
				if ( min<= a->getLength()  && max>= a->getLength())
					result.push_back(a);
		 }
		 
		 
		 }
		 
		  
		//for (v=data->setOfVectors.begin(); v!=data->setOfVectors.end(); v++)
	 	 if (bin) { 
		 if(Constants::debug)std::cout<<"szukanie bin"<<std::endl;
		 //data->sortWRTLength();

		int l=0; //pocz
		int p=il; //koniec
		int s =0; //srodek
		int pom=0;
		
		 do {
			 max_w++;
			 if (l>p) {if(Constants::debug) std::cout<<"nie znaleziono"<<std::endl;}
			 else {
				 s= (l+p) /2;
				 
			 
			 }
			a =data->setOfVectors[s];
				if ( min<= a->getLength()  && max >= a->getLength()) 
				{				//std::cout<<"znaleziony kandydat"<<std::endl;
					result.push_back(a); 
					  
					//for ( *v =data->setOfVectors[s-1] ; v!= data->setOfVectors.begin() ; v-- ) //w dol od obecnego p
					for (pom =(s-1) ;pom >l ;pom--) 	{			//	std::cout<<"szukam nizej"<<std::endl;
						if  (  min <= data->setOfVectors[pom]->getLength()) {  //poki nalezy do przedzialu
						result.push_back(data->setOfVectors[pom]);
						  }
						else  break;
					
					};

					for (pom= (s+1) ;pom<p ;pom++) 	{	//std::cout<<"szukam wyzej"<<std::endl;
						if  (  max >= data->setOfVectors[pom]->getLength()) { 	//	std::cout<<"dodaje wekt nr "<< pom+1 <<std::endl;
						result.push_back(data->setOfVectors[pom]);
						  }
						else break;
					
					};
		}
		else if (min> a->getLength() )

		{
			l=s+1; 
		
		}

		else if (max< a->getLength() ){
		 p=s-1;
		
		}
		if (max_w ==il) break;
		 }
		 while ( result.empty());

		  
				  
		
		 }
			 
			 candidates=result;
		 
		//  system ("pause");
		return candidates;
	}

};
#endif