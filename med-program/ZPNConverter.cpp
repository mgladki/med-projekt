#include "Data.cpp"

using namespace std;

class ZPNConverter {
	
private:
	Data * data;

public:

	ZPNConverter(Data * data){
		this->data = data;
	}

	/* Konwersja */
	void convert(){

		for(vector<Vector*>::iterator it1=data->setOfVectors.begin(); it1!=data->setOfVectors.end();it1++){
			hash_map<int, int> tempValues = (*it1)->getSetOfValues();
			for(hash_map<int, int>::iterator it2=tempValues.begin(); it2!=tempValues.end();it2++){
					if(it2->second ==1)
						it2->second=-1;
					else if(it2->second>1)
						it2->second=1;
					else if(it2->second<-1)
						it2->second=-1;
			}
		}
	}
};