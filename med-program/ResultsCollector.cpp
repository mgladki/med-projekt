#include <fstream>
#include "Resolver.cpp"

#define WINDOW_WIDTH 80

using namespace std;

class ResultsCollector {
private:
	string fileName;
	int sourceSize;

	struct ResultsForGivenMeasure
	{
		vector<vector<Vector*>> bol;
		float bolTime;
		vector<vector<Vector*>> j;
		float jTime;
		vector<vector<Vector*>> bolJ;
		float bolJTime;
		vector<vector<Vector*>> bolJL;
		float bolJLTime;
		vector<vector<Vector*>> real;
		float realTime;
	};
	struct ResultsForGivenEps{
		ResultsForGivenMeasure cos;
		ResultsForGivenMeasure tanim;
	};
	hash_map<double,ResultsForGivenEps> results;
public:

	ResultsCollector(string fileName, int inputDataSize){
		this->fileName = fileName;
		sourceSize=inputDataSize;
	}

	int calculateNumberOfElements(const vector<vector<Vector*>> vec) const
	{
		int result=0;

		for(int i=0;i<vec.size();i++)
			result+=vec[i].size();

		return result;
	}

	/*void printToConsole(){
		int baseWidth=WINDOW_WIDTH-1;

		cout.setf(ios::fixed, ios::floatfield);
		cout.precision(2);
		for(hash_map<double,ResultsForGivenEps>::iterator it=results.begin();it!=results.end();it++)
		{
			int bolCandidCount=-1;
			int jCandidCount=-1;
			int bolJCandidCount=-1;
			int bolJLCandidCount=-1;
			int cosCandidCount=-1;
			int tanimCandidCount=-1;

			cout<<"*****************"<<endl;
			cout<<"For epsilon = "<<it->first<<endl;
			cout<<"*****************"<<endl;

			cout<<"COSINE RESULTS"<<endl<<endl;
			
			for(int i=0;i<baseWidth;i++)
				cout<<"-";
			cout<<endl;
			if(it->second.cos.bol.size()>0)
			{
				bolCandidCount=calculateNumberOfElements(it->second.bol);

				for(int i=0;i<baseWidth*bolCandidCount/sourceSize;i++)
					cout<<"b";
				cout<<endl;
			}
			if(it->second.cos.bol.size()>0)
			{
				jCandidCount=calculateNumberOfElements(it->second.j);

				for(int i=0;i<baseWidth*jCandidCount/sourceSize;i++)
					cout<<"j";
				cout<<endl;
			}
			if(it->second.cos.bolJ.size()>0)
			{
				bolJCandidCount=calculateNumberOfElements(it->second.bolJ);

				for(int i=0;i<baseWidth*bolJCandidCount/sourceSize;i++)
					cout<<"2";
				cout<<endl;
			}
			if(it->second.cos.bolJL.size()>0)
			{
				bolJLCandidCount=calculateNumberOfElements(it->second.bolJL);

				for(int i=0;i<baseWidth*bolJLCandidCount/sourceSize;i++)
					cout<<"3";
				cout<<endl;
			}
			if(it->second.cos.real.size()>0)
			{
				cosCandidCount=calculateNumberOfElements(it->second.cos);

				for(int i=0;i<baseWidth*cosCandidCount/sourceSize;i++)
					cout<<"c";
				cout<<endl;
			}
			if(it->second.tanim.size()>0)
			{
				tanimCandidCount=calculateNumberOfElements(it->second.tanim);

				for(int i=0;i<baseWidth*tanimCandidCount/sourceSize;i++)
					cout<<"t";
				cout<<endl;
			}
			cout<<endl;
			cout<<"                     REMAINING         COMPUTED IN [S]"<<endl;
			cout<<"            OldBnds  "<<endl;
			cout<<"                NZD  "<<endl;
			cout<<"        OldBnds&NZD  "<<endl;
			cout<<"OldBnds&NZD&NewBnds  "<<endl;
			cout<<"             Cosine  "<<endl;
			cout<<endl;
		}
	}*/

	void printToFile(){

	}

	/*void addBolResult(vector<vector<Vector*>> &val){
		results[Resolver::epsilon].bol=val;
	}

	void addJResult(vector<vector<Vector*>> &val){
		results[Resolver::epsilon].j=val;
	}

	void addBolJResult(vector<vector<Vector*>> &val){
		results[Resolver::epsilon].bolJ=val;
	}

	void addBolJLResult(vector<vector<Vector*>> &val){
		results[Resolver::epsilon].bolJL=val;
	}

	void addCosResult(vector<vector<Vector*>> &val){
		results[Resolver::epsilon].cos=val;
	}

	void addTanimResult(vector<vector<Vector*>> &val){
		results[Resolver::epsilon].tanim=val;
	}*/
};