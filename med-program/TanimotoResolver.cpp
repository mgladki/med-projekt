#ifndef TR_CPP
#define TR_CPP


#include "Data.cpp";
#include "Vector.cpp";
#include "Resolver.cpp";
#include <vector>

/*
*	author: Gregdeg
*/
class TanimotoResolver: public Resolver{


public:
	TanimotoResolver(Data * data){
		this->data = data;
	}

	std::vector< Vector *> findSimilar(Vector* vector){
		std::cout << "Searching for similar vectors using Tanimoto method\n";
		std::vector<Vector*> candidates;
		for(std::vector<Vector*>::size_type i=0;
			i<data->setOfVectors.size();i++){
				if(tanimotoSimilarity(vector, data->setOfVectors[i])>=epsilon)
					candidates.push_back(data->setOfVectors[i]);
		}

		return candidates; //TODO fill candidates
	}

	static double tanimotoSimilarity(Vector *v1, Vector *v2){
		int dp=v1->dotProduct(v2);

		return dp/(v1->getLength()*v1->getLength()+v2->getLength()*v2->getLength()-dp);
	}
};

#endif