﻿#ifndef LR_CPP
#define LR_CPP

#include "Data.cpp";
#include "Vector.cpp";
#include "Resolver.cpp";
#include "BoundsOfLengthResolver.cpp";
#include "JResolver.cpp";

#include <hash_map>
#include <vector>
#include <string>
#include <cstdlib>

struct Group {

public:
	int size;
	std::vector<int> lDimensions;
	std::vector<Vector *> lVectors;

	Group(int size, std::vector<int> lDimensions){
		this->size = size;
		this->lDimensions = lDimensions;
	}

	bool addVector(Vector * vec, std::vector<int> lDimensions){
		for(int i = 0; i< size; i ++){
			if(this->lDimensions[i] != lDimensions[i]) return false;
		}
		//std::cout<<"added vector " << vec->getIdentifier() << " to existing group\n";
		lVectors.push_back(vec);
		return true;
	}
	
};

struct GroupHelper {
	// kluczem jest dlugosc grupy, wartościa lista wskaźników na grupy
	std::hash_map<int,std::vector<Group *> *> groups;



public:

	void addVectorToGroup(Vector * vec, std::vector<int> lDimensions){
		//std::cout<<"Adding vec with ldim size : " << lDimensions.size() << "\n";
		//std::cout<<"There is  : " << groups[lDimensions.size()]->size() << " groups with this alsdjf\n";
		if(groups.find(lDimensions.size()) != groups.end() && groups.find(lDimensions.size())->second->size() > 0 ){
			//std::cout<<"Istnieje juz "<<groups[lDimensions.size()]->size()<<" grup o danej dlugosci [" << lDimensions.size() << "]\n";
			bool added = false;
			int i = 0;
			while(added == false && i < groups[lDimensions.size()]->size()){
				//std::cout<<"--->Trying to add vector to " << i << " group \n";
				added = ((*(groups.find(lDimensions.size()))->second)[i])->addVector(vec,lDimensions);
				i++;
			}
		}
		else{
			//std::cout<<"Nie istnieje jeszcze grupa o danej dlugosci [" << lDimensions.size() << "], tworzenie...\n";
			Group * newGroup = new Group(lDimensions.size(), lDimensions );
			newGroup->lVectors.push_back(vec);
			std::vector<Group *> * newGroups = new std::vector<Group *>();
			newGroups->push_back(newGroup);
			groups.insert(std::pair<int, std::vector<Group *>*>(lDimensions.size(), newGroups));
		}
	}
    
};



/*
*	author: Karolina Przerwa
*/
class LResolver: public Resolver{


public:
	LResolver(Data * data, JResolver * jr , BoundsOfLengthResolver * blr ){
		this->data = data;
		this->jr= jr;
		this->blr =blr;
	}

	std::vector< Vector *> findSimilar(Vector* vector){
		std::cout << "Searching for similar vectors using L method\n";
		//std::cout << "\t[Not Yet Implemented]\n";
		candidates.clear();
		
		this->gh = new GroupHelper();

		std::vector<Vector *> blrResult = blr->candidatesAll[curr];
		std::vector<Vector *> jrResult = jr->candidatesAll[curr];
		std::vector<Vector *> inputResult;
		std::vector<int> jSet;
		std::hash_map<Vector *, std::vector<int>> lSet;
		std::vector<Vector *> result;


		/* obliczenie czesci wspolnej dla bounds i J */
		//std::cout << "Calculating intersection of oldBnds and J...\n";
		for (std::vector<Vector*>::iterator it = blrResult.begin() ; it!=blrResult.end(); it++){

			std::string a= (*it)->getIdentifier();// candidates1[1]->getIdentifier();//   
			for (std::vector<Vector*>::iterator it2=jrResult.begin(); it2!=jrResult.end() ;it2++){
				std::string b=(*it2)->getIdentifier();
				if (a==b) {
					inputResult.push_back ( *it);
					break;
				}
			}
		}

		//std::cout<<"intersection of sets: " << inputResult.size() << "\n";

		/* pobranie zbiour J */
		jSet = jr->getJSet();

		/* kontrola */
		if(Constants::debug  ){
			std::cout << "\njSet: [";
			for(int i =0; i< jSet.size(); i++){
				std::cout<<jSet[i] << " ";
			}
			std::cout << "]\n";
		}
		if(Constants::debug  ){
			std::cout << "Intersected result: [";
			for(int i =0; i< inputResult.size(); i++){
				std::cout<<inputResult[i]->getIdentifier() << " ";
			}
			std::cout << "]\n";
		}

		/* wypelnienie zbioru L */	
		//std::cout << "Calculating L set...\t";
		std::hash_map<int,int> current;
		for(std::vector<Vector*>::iterator it = inputResult.begin() ; it!=inputResult.end(); it++){
			current = (*it)->getSetOfValues();
			std::vector<int> currentSet;
			for(int j = 0; j< jSet.size(); j++){
				//int i = (*it2);
				if(current.count(jSet[j]) == 0){
					currentSet.push_back(jSet[j]);
				}
			}

			//std::string id = (*it)->getIdentifier();
			gh->addVectorToGroup((*it),currentSet);
			//lSet.insert(std::pair<Vector *,std::vector<int>>((*it), currentSet));
		}
		//std::cout << "L set calculated, has "<< lSet.size() << " elements\n";

		/*kontrola zbioru L*/
		if(Constants::debug  ){
			for(std::hash_map<Vector *, std::vector<int>>::iterator it = lSet.begin() ; it!=lSet.end(); it++){
				std::cout<<"\n" << (*it).first->getIdentifier() << "\t";
				std::vector<int> currentSet = (*it).second;

				for(int j = 0; j< currentSet.size() ; j++){
					std::cout<<" "<<currentSet[j];
				}
			}
		}


			std::vector<Group *> * currentGroups;
			std::vector<Vector * > * tmp;
			for(std::hash_map<int,std::vector<Group *> *>::iterator it = gh->groups.begin(); it!=gh->groups.end(); it++){

					currentGroups = (*it).second;
			
				//std::cout<<"\n Current grup size = " << currentGroups->at(0)->size<< "\n";
				

				for(int j = 0 ; j< currentGroups->size(); j++){
					
					/*
					std::cout<<"--> Current L set is {";
					for(int k = 0 ; k< currentGroups->at(j)->lDimensions.size(); k++){
						std::cout<< " " << currentGroups->at(j)->lDimensions.at(k);
					}
					std::cout<<"} \n";

					
					std::cout<<"--> For vectors {";
					for(int k = 0 ; k< currentGroups->at(j)->lVectors.size(); k++){
						std::cout<< " " << currentGroups->at(j)->lVectors.at(k)->getIdentifier();
					}
					std::cout<<"} \n";
					*/

					
					tmp = meetsCondition(currentGroups->at(j)->lVectors, currentGroups->at(j)->lDimensions, vector);
					for(std::vector<Vector *>::iterator itf = result.begin(); itf != result.end(); itf++){
						bool add = true;
						for(std::vector<Vector *> ::iterator itf2 = tmp->begin(); itf2 != tmp->end(); itf2++){
							if((*itf)->getIdentifier() == (*itf2)->getIdentifier()){
								result.erase(itf);
							}
						}
					}
					result.insert(result.end(), tmp->begin(), tmp->end());
					/*
					std::cout<<"--> Fittable Vectors are {";
					for(int k = 0 ; k< tmp->size(); k++){
						std::cout<< " " << tmp->at(k)->getIdentifier();
					}
					std::cout<<"} \n";
					*/
					tmp->clear();
				}
			}
		




		if(Constants::debug ){
			std::cout<<"Result [";
			for(int i=0; i<result.size(); i++){
				std::cout<<" " << result[i]->getIdentifier();
			}
			std::cout<<"]";
		}
		candidates = result;

		return candidates; //TODO fill candidates
	}

	std::vector<Vector *> * meetsCondition(std::vector<Vector *> lvectors,std::vector<int> vectorLSet, Vector * currVecOfInterest){
		if(Constants::debug ) std::cout << "\nChecking L conditions, L=[";
		std::vector<Vector*> *coresult = new std::vector<Vector*>();
		double sum = 0;
		double upperBound;
		int ui ;
		double ul = currVecOfInterest->getLength();
		//std::cout << "->Checking conditions...\n";
		for(int i = 0; i< vectorLSet.size(); i++){
			if(Constants::debug ) std::cout << " " << vectorLSet[i];
			ui = currVecOfInterest->getSetOfValues()[vectorLSet[i]];

			sum += (ui/ul)*(ui/ul);
		}

		upperBound = (1-sum)*ul/epsilon;
		


		//std::cout << "UpperBound = " << upperBound << " calculated once\n";
		//std::cout<<"Checking vectors if suitable for upper bound...\t";
		for(std::vector<Vector *>::iterator it = lvectors.begin(); it != lvectors.end(); it++ ){
			if((*it)->getLength() <= upperBound){
				//std::cout<<(*it)->getIdentifier() << " ";
				coresult->push_back((*it));
		
			}
		}
		//std::cout<< std::endl;
		return coresult;
	}

private:
	JResolver * jr;
	BoundsOfLengthResolver * blr;
	double uPrimCount(int ui, double ulength){
		return ((double)ui)/ulength;
	}

	GroupHelper * gh;

};
#endif