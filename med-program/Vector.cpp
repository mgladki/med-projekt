#ifndef VECTOR_CPP
#define VECTOR_CPP

#include <cmath>
#include <string>
#include <algorithm> 
#include <hash_map>
#include <set>
#include <iostream>

#include "Pair.cpp";
#include "Constants.cpp";

class Data;
class Pair;

struct less_second {
	std::hash_map<int,int> * newOrder;
    typedef std::pair<int, int> type;
    bool operator ()(type const& a, type const& b) const {
		/*std::cout<<"Is a={"<<a.first<<","<<a.second<<"} bigger than b={"<<b.first<<","<<b.second<<"}\n";
		std::cout<<"--->newOrder size is " << newOrder->size() <<" \n ";
		std::cout<<"--->is "<<newOrder->find(a.first)->second <<" > " << newOrder->find(b.first)->second << "?\n";*/
		return newOrder->find(a.first)->second < newOrder->find(b.first)->second;
    }

	/*bool operator ()(type const& a, type const& b) const {
		return a.first > b.first;
    }*/
	less_second(std::hash_map<int,int> * newOrder){
		this->newOrder = newOrder;
	}
};

/*
*	author: Maciej Gladki
*/
class Vector {

private:
	std::string identifier;
	double length;
	std::hash_map< int,int > setOfValues;
	std::hash_map< int,int > newSetOfValues;

public:

	Vector(std::string identifier){
		length = -1;
		this->identifier = identifier;
		from = 0;
	}

	/* Oblicza dlugosc wektora w sposob leniwy */
	double getLength()
	{
		/* przypadek gdy dlugosc nie zostala jeszcze obliczona */
		if (length = -1){
			double lengthSq = 0;
			for(std::hash_map<int,int>::iterator iter = setOfValues.begin(); iter != setOfValues.end(); ++iter){
				lengthSq += pow(iter->second,2.0);
			}
			length = sqrt(lengthSq);
		}
		return length;
	}

	int dotProduct(Vector * that){
		if(Constants::debug) std::cout << "\n--->Counting dotProduct\n"; 
		int result = 0;
		std::vector<int> NZDOfThis, NZDOfThat;
		std::vector<int>::iterator vecIt;
		int* NZDOfThisTab;
		int* NZDOfThatTab;
		int maxIntersectionSize;

		//std::cout << "\n--->Vector 1 to process:\n------>";

		for(std::hash_map<int,int>::iterator it = this->setOfValues.begin(); it != this->setOfValues.end(); ++it) {
			NZDOfThis.push_back(it->first);
			//std::cout << it->first << " " ;
		}
		NZDOfThisTab = &NZDOfThis[0];

		//std::cout << "\n--->Vector 2 to process:\n------>";
		for(std::hash_map<int,int>::iterator it = that->setOfValues.begin(); it != that->setOfValues.end(); ++it) {
			NZDOfThat.push_back(it->first);
			//std::cout << it->first << " " ;
		}
		NZDOfThatTab = &NZDOfThat[0];

		int a = NZDOfThis.size();
		int b = NZDOfThat.size();

		std::sort(NZDOfThisTab, NZDOfThisTab + a);
		std::sort(NZDOfThatTab, NZDOfThatTab + b);

		maxIntersectionSize  = !(b<a)?a:b;
		std::vector<int> intersection(100);

		vecIt = std::set_intersection(NZDOfThisTab, NZDOfThisTab + NZDOfThis.size(), NZDOfThatTab, NZDOfThatTab + NZDOfThat.size(),intersection.begin());
		intersection.resize(vecIt - intersection.begin()); 

		if(Constants::debug)std::cout << "\n--->Vectors have " << (intersection.size()) << " common dimensions:\n";
		for (vecIt=intersection.begin(); vecIt!=intersection.end(); ++vecIt){
			if(Constants::debug) std::cout << ' ' << *vecIt;
			result += this->setOfValues[*vecIt] * that->setOfValues[*vecIt];
		}
		if(Constants::debug) std::cout << "\n--->Dot product is " << result << std::endl;

		return result;
	}

	void getNZD()
	{
	}

	void Vector::getZD()
	{
	}

	std::string getIdentifier(){
		return this->identifier;
	}

	void addValue(int dimension, int value){
		setOfValues[dimension] = value;
	}

	std::hash_map<int, int> getSetOfValues(){
		return setOfValues;
	}

	/*Sortowanie wymiar�w po ich cz�sto�ci wsrod wszystkich wektorow */
	void sortByFreqOfDim(std::hash_map<int,int> * newOrder){
		if(setOfValues.size()<1)
			return;

		else {
			std::vector<std::pair<int, int> > mapcopy(setOfValues.begin(), setOfValues.end());
			std::sort(mapcopy.begin(), mapcopy.end(), less_second(newOrder));
			std::hash_map<int,int> newSetOfValues(mapcopy.begin(),mapcopy.end());

			setOfValues.clear(); 
			setOfValues = newSetOfValues;
		}
		
	}

	std::hash_map<int,int> *newOrder;
	
	void setNewOrder(std::hash_map<int,int> *newOrder){
		this->newOrder= newOrder;
	}

	int from;
	
	

/*
	struct sort_pred {
    bool operator()(const std::pair<int,int> &left, const std::pair<int,int> &right) {
        return left.second < right.second;
    }
};
*/

	//struct CompareByInvInd {
	//	CompareByInvInd(std::vector< std::vector<Vector *>> &ii) : invInd(ii) {}
	//	bool operator()(const Pair a, const Pair b) {
	//		return invInd[a.dimension].size() > invInd[b.dimension].size();
	//	}
	//private:
	//	std::vector< std::vector<Vector *>> &invInd;
	//};
};





#endif