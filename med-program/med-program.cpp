﻿// med-program.cpp : Defines the entry point for the console application.
//testt
#pragma execution_character_set("utf-8")
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <windows.h>
#include <string>
#include <cstdlib>
#include <ctime>

/* podstawowe struktury */
#include "Data.cpp";
#include "Vector.cpp";  
#include "FileLoader.cpp";

/* resolvery do szukania podobnych wektorow */
#include "Resolver.cpp";
#include "BruteForceResolver.cpp";
#include "CosinResolver.cpp";
#include "TanimotoResolver.cpp";
#include "JResolver.cpp";
#include "LResolver.cpp";
#include "BoundsOfLengthResolver.cpp";

double Resolver::epsilon;
fstream Resolver::theFile;
bool FileLoader::needToConvertToZPN;

/*
 *	authors: Karolina Przerwa, monika w, Gregdeg, Maciej Gladki
 */


void xcandidatesAll (std::vector<std::vector< Vector *> > candidatesAll1, std::vector<std::vector< Vector *> > candidatesAll2){
	 
	std::cout<<"searching for candidates meeting 2 criteria" <<std::endl;
	std::vector< Vector *>  wynik;
	std::vector<std::vector< Vector *> > candidateswynik;
	int size=0;         
	// Vector * z;
	int size_all=candidatesAll1.size();

	//std::cout<<"intersection of two sets, size of 1 is " << candidatesAll1.size() << " and size of 2 is " << candidatesAll2.size() << " elements"<<std::endl;

	for (int i=0;i<size_all;i++){
	for ( std::vector< Vector *>  ::iterator it = candidatesAll1[i].begin() ; it!=candidatesAll1[i].end(); it++){
		
		string a= (*it)->getIdentifier();// candidates1[1]->getIdentifier();//   
		for (std::vector<Vector*>::iterator it2=candidatesAll2[i].begin(); it2!=candidatesAll2[i].end() ;it2++){
			string b=(*it2)->getIdentifier();
			if (a==b) {
			
				wynik.push_back ( *it);
				break;
			}
		}
		 
	 
		}
	candidateswynik.push_back(wynik);
	wynik.clear();// wazne, sumuje sie przy kolejnych wykonaniach bez tego
	}

	std::cout<<"intersection size is " << candidateswynik.size() << " elements"  << endl;
	
	//druk
	
	for (int j=0;j<candidateswynik.size();j++){
			candidateswynik[j];
		if(candidateswynik[j].size() >0){
			std::cout << "\t[Result ready] Number of candidates: " << candidateswynik[j].size() << "\n";
			if(candidateswynik[j].size() <= 10){
				std::cout << "\tCandidates are:\n\t";
				for(std::vector<Vector*>::iterator it = candidateswynik[j].begin(); it != candidateswynik[j].end(); it++) {
					Vector *currentVector = *it;
					std::cout << " " << currentVector->getIdentifier();
				}
			std::cout<<"\n";
			}
			
		}
	}
}
	void xcandidates (std::vector< Vector *>  candidates1, std::vector< Vector *>  candidates2){
		std::vector<std::vector< Vector *> > candidatesAll;
	std::cout<<"searching for candidates meeting 2 criteria" <<std::endl;
	std::vector< Vector *>  wynik;
	int size=0;         
	// Vector * z;
	
	for (std::vector<Vector*>::iterator it = candidates1.begin() ; it!=candidates1.end(); it++){
		
		string a= (*it)->getIdentifier();// candidates1[1]->getIdentifier();//   
		for (std::vector<Vector*>::iterator it2=candidates2.begin(); it2!=candidates2.end() ;it2++){
			string b=(*it2)->getIdentifier();
			if (a==b) {
			
				wynik.push_back ( *it);
				break;
			}
		}
		}


	//druk 

	if(wynik.size() >0){
			std::cout << "\t[Result ready] Candidates are:\n\t";
			for(std::vector<Vector*>::iterator it = wynik.begin(); it != wynik.end(); it++) {
				Vector *currentVector = *it;
				std::cout << " " << currentVector->getIdentifier();
			}
		} else{
			std::cout << "\tNo candidates";
		}
		std::cout<< std::endl;

	}
	
	
int main(int argc, _TCHAR* argv[])
	
{	
	// Przygotowanie zbioru wsrod ktorego szukamy podobnych wektorow 
	// -------------------------------------------------------------
	
	/* stworzenie struktury danych dla zbioru */
	Data *data = new Data();
	
	/* stworzenie struktury odpowiadajacej za odczyt pliku i wypelnianie danych */
	FileLoader *fileloader = new FileLoader(data);
	
	string name;
	cout<<"Podaj nazwe pliku ze zbiorem potencjalnych kandydatow"<<endl;
	cin >> name;
	cout<<"Konwertowac na ZPN [t/n]? ";
	cin.ignore();
	FileLoader::needToConvertToZPN=(cin.get()=='t')?true:false;
	
	/* odczytanie danych z pliku */
	fileloader->loadDataFromFile(name);
	

	// Przygotowanie zbioru wsrod ktorego szukamy podobnych wektorow 
	// -------------------------------------------------------------
	
	/* stworzenie struktury danych dla zbioru */
	Data *dataWzor = new Data();
	
	/* stworzenie struktury odpowiadajacej za odczyt pliku i wypelnianie danych */
	FileLoader *fileloaderwzor = new FileLoader(dataWzor); 
	
	string namewzor;
	cout<<"Podaj nazwe pliku ze zbiorem wektorow dla ktorych szukamy kandydatow"<<endl;
	 cin >> namewzor;
	cout<<"Konwertowac na ZPN [t/n]? ";
	cin.ignore();
	FileLoader::needToConvertToZPN=(cin.get()=='t')?true:false;
	
	/* odczytanie danych z pliku */
	fileloaderwzor->loadDataFromFile(namewzor);
	
	
	/* ustawienie wektora wzorcowego jako pierwszy wektor z wczytanych danych */
	int randomVectorOfInterest = rand() % data->setOfVectors.size();
	//data->vectorOfInterest = dataWzor->setOfVectors[0];

	// Zapis do pliku
		Resolver::theFile.open("zpnOutput.txt",std::ios::app);
	if(!Resolver::theFile.good())
		{
		std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
		}
	else{
		  time_t rawtime;
		  struct tm * timeinfo;

		  time (&rawtime);
          timeinfo = localtime (&rawtime);
		  Resolver::theFile<< "RUNTIME TIMESTAMP: " << asctime(timeinfo);
		  Resolver::theFile.close();
	}
	
	CosinResolver *csr = new CosinResolver(data);
	TanimotoResolver *tnr = new TanimotoResolver(data);
	BoundsOfLengthResolver *blr = new BoundsOfLengthResolver(data);
	JResolver *jr = new JResolver(data);
	LResolver *lr = new LResolver(data,jr,blr);
		
	
	int wybor=0;
	bool menu =true;
	cout<<"Near Duplicates among Vectors with Domains Consisting of Zero, a Positive Number and a NEgative Number"<<endl;
	/* pobranie warto?ci epsilona od u?ytkownika */
	bool inputAccepted=false;
	
	/* wypisanie wczytanych danych */
	std::cout << data->print();
	std::cout << std::endl;

	do
	{
	std::cout << "Enter an epsilon: ";
	std::cin >> Resolver::epsilon;
	inputAccepted=std::cin.good();
	std::cin.clear();
	std::cin.sync();
	if(!inputAccepted)
		std::cout << "Invalid value" << std::endl;
	}
	while(!inputAccepted);

	
	while (menu) {
		cout << endl << "Wybor trybu pracy programu: \n #0 Metoda bruteforce: Cosine\n #1 Metoda bruteforce: Tanimoto\n #2 Metoda ograniczania dlugosci wektorow (Cosine) \n #3 Metoda ograniczania dlugosci wektorow (Tanimoto) \n #4 Oblicz inverted indices\n #5 Metoda J\n #6 Metoda L\n #7 Metoda L (Tanimoto)\n #8 Wyniki wspolne dla bounds on lenghts i J\n #9 Zmien epsilon\n #10 Test calosci  \n #11 Koniec\n"  << endl;
	cin>>wybor;

	switch (wybor)
	{
	
	case 0:
		{ 
			/* szukanie podobnych wektorow metoda podobienstwa cosinusowego */
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"COSINE DIRECT SEARCH\n\n";
				Resolver::theFile.close();
				}
			csr->printResult(dataWzor);
			  break;
		} 
	case 1:
		{
			/* szukanie podobnych wektorow metoda podobienstwa tanimoto */
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"TANIMOTO DIRECT SEARCH\n\n";
				Resolver::theFile.close();
			}
			tnr->printResult(dataWzor);
			break;
		}
	case 2:
		{
			/* szukanie podobnych wektorow metoda bounds of length */
			
			blr->ifCosine = 1;
			int menu2=0;
			while (!menu2) {
			cout<<"Wyszukiwanie binarne:wpisz 1" <<endl;
			cout<<"Wyszukiwanie po wszystkich:wpisz 2" <<endl;
			int a;
			cin>>a;
			if (a==1)  {blr->bin=true;  data->sortWRTLength(); break;}
			else if(a==2){ blr->bin=false; break;}
			 else cout<< "bledna opcja, jeszcze raz"<<endl;
			}
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"BOUNDS OF LENGTH\n\n";
				Resolver::theFile.close();
			}
			blr->printResult(dataWzor);
			break;
		
		}
	case 3:{
	   /* szukanie podobnych wektorow metoda bounds of length tanimoto */
			
			blr->ifCosine = 0;
			int menu2=0;
			while (!menu2) {
			cout<<"Wyszukiwanie binarne:wpisz 1" <<endl;
			cout<<"Wyszukiwanie po wszystkich:wpisz 2" <<endl;
			int a;
			cin>>a;
			if (a==1)  {blr->bin=true;  data->sortWRTLength(); break;}
			else if(a==2){ blr->bin=false; break;}
			 else cout<< "bledna opcja, jeszcze raz"<<endl;
			 
			}
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"BOUNDS OF LENGTH\n\n";
				Resolver::theFile.close();
			}
			blr->printResult(dataWzor);
			break;
		
	   
	   }
	case 4:
		{
			std::cout << "Calculating inverted indices and sort by dimensions frequency ...\n";
			data->calculateInvertedInd();
			std::cout << "the same for wzor ...\n";
			//dataWzor->calculateInvertedInd();

			std::vector<Pair *> dims=data->orderDimensions2();			
			dataWzor->calculateInvertedInd2(dims);
			std::cout << "uniewaznianie wynikow ...\n";
			//blr->isResultAvailable = false;
			//csr->isResultAvailable = false;
			//tnr->isResultAvailable = false;
			jr->isResultAvailable  = false;
			lr->isResultAvailable  = false;
			break;
		}
	case 5:
		{
		/* szukanie podobnych wektorow metoda wyliczenia wspolczynnika J */
		std::cout << "Resolving with J method\n";

		Resolver::theFile.open("zpnOutput.txt",std::ios::app);
		if(!Resolver::theFile.good())
			{
			std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
			}
		else{
		Resolver::theFile<<"J\n\n";
		Resolver::theFile.close();
		}
		jr->printResult(dataWzor);
		break; 
		}
	case 6:
		{
			if(!blr->isResultAvailable){
				std::cout<<"Bounds of length result unavailable\n";
				break;
			}
			
			if(!jr->isResultAvailable){
				std::cout<<"J method result unavailable\n";
				break;
			}
		/* szukanie podobnych wektorow metoda wyliczenia wspolczynnika L */
		std::cout << "Resolving with L method\n";
		lr->ifCosine = 1;

		Resolver::theFile.open("zpnOutput.txt",std::ios::app);
		if(!Resolver::theFile.good())
			{
			std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
			}
		else{
			Resolver::theFile<<"L\n\n";
			Resolver::theFile.close();
		}
		lr->printResult(dataWzor);
		break;
		

		}
		case 7:
		{
			if(!blr->isResultAvailable){
				std::cout<<"Bounds of length result unavailable\n";
				break;
			}
			
			if(!jr->isResultAvailable){
				std::cout<<"J method result unavailable\n";
				break;
			}
		/* szukanie podobnych wektorow metoda wyliczenia wspolczynnika L */
		std::cout << "Resolving with L method\n";
		lr->ifCosine = 0;

		Resolver::theFile.open("zpnOutput.txt",std::ios::app);
		if(!Resolver::theFile.good())
			{
			std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
			}
		else{
			Resolver::theFile<<"L\n\n";
			Resolver::theFile.close();
		}
		lr->printResult(dataWzor);
		break;
		

		}
	case 8:
		{	//czesc wspolna J i blr tego co wczesniej odpalone
			if(!blr->isResultAvailable){
				std::cout<<"Bounds of length result unavailable\n";
				break;
			}
			
			if(!jr->isResultAvailable){
				std::cout<<"J method result unavailable\n";
				break;
			}

			/*BoundsOfLengthResolver *blr = new BoundsOfLengthResolver(data);
			blr->ifCosine = 1;
			blr->printResult(dataWzor);
			JResolver *jr = new JResolver(data);
			jr->printResult(dataWzor);*/
			xcandidatesAll(blr->candidatesAll,jr->candidatesAll);
			//xcandidates(blr->candidates,jr->candidates);
			break;
		}
	case 9:
		{
			bool inputAccepted=false;
			do
			{
				std::cout << "Enter an epsilon: ";
				std::cin >> Resolver::epsilon;
				inputAccepted=std::cin.good();
				std::cin.clear();
				std::cin.sync();
				if(!inputAccepted)
				std::cout << "Invalid value" << std::endl;
				blr->isResultAvailable = false;
				csr->isResultAvailable = false;
				tnr->isResultAvailable = false;
				jr->isResultAvailable  = false;
				lr->isResultAvailable  = false;
			}
			while(!inputAccepted);
		 
			break;
		}
	
	case 10:
		{
			/* obliczenie wykazu odwroconych indeksow */
			data->calculateInvertedInd(); 
	
			/* szukanie podobnych wektorow metoda brute force */
			//bfr->printResult(); // TODO: nalezy nalozyc ograniczenie czasowe
	
	
			/* szukanie podobnych wektorow metoda podobienstwa tanimoto */
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"TANIMOTO DIRECT SEARCH\n\n";
				Resolver::theFile.close();
			}
			tnr->printResult(dataWzor);
	
			/* szukanie podobnych wektorow metoda bounds of length */
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"BOUNDS OF LENGTH\n\n";
				Resolver::theFile.close();
			}
			blr->printResult(dataWzor);
	
			/* szukanie podobnych wektorow metoda wyliczenia wspolczynnika J */
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"J\n\n";
				Resolver::theFile.close();
			}
			jr->printResult(dataWzor);
	
			/* szukanie podobnych wektorow metoda wyliczenia wspolczynnika L */
			Resolver::theFile.open("zpnOutput.txt",std::ios::app);
			if(!Resolver::theFile.good())
				{
				std::cout<<"Failed to open the output file"<<std::endl<<std::endl;
				}
			else{
				Resolver::theFile<<"L\n\n";
				Resolver::theFile.close();
			}
			lr->printResult(dataWzor);
	
			xcandidates(blr->candidates,jr->candidates);
		

	

		
		
		break;
		}

	case 99:
		{
			randomVectorOfInterest = rand() % data->setOfVectors.size();
			data->vectorOfInterest = data->setOfVectors[randomVectorOfInterest];
			blr->isResultAvailable = false;
			csr->isResultAvailable = false;
			tnr->isResultAvailable = false;
			jr->isResultAvailable  = false;
			lr->isResultAvailable  = false;
			break;
		}
		case 69:
		{
			std::cout<<data->print();
			break;
		}


	case 11:
		{
			menu=false;
			Resolver::theFile.close();
			break;
		}
		
	default:
		cout << endl << "Niepoprawny wybor."<<endl;
		break;
	}
	 std::cin.clear();
	}
	 



	
	/*tymczasowy test dot-product */
	/*std::cout << "Temporary dot-product test (vector1*vector2)\n";
	data->setOfVectors[0]->dotProduct(data->setOfVectors[1]);
	*/
	/* */
	//system("pause");
	return 0;
}