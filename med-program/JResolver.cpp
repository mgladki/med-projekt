#ifndef JR_CPP
#define JR_CPP

#include <vector>

#include "Data.cpp";
#include "Vector.cpp";
#include "Resolver.cpp";
#include "CosinResolver.cpp";
/*
*	author: monika w
*/
class JResolver : public Resolver{

private:
	std::vector <int> valid_dimensions;

public:
	JResolver(Data * data){
		this->data = data;
	}

	std::vector< Vector *> findSimilar(Vector* vector){
		std::cout << "Searching for similar vectors using J method\n";
		//std::cout << "\t[Not Yet Implemented]\n";
		Vector* u; //nasze u
		valid_dimensions;
		std::vector<Vector *> :: iterator vd;
		std::hash_map<int,int>  sets; // = this->setOfValues.begin(); it != this->setOfValues.end(); ++it
		std::hash_map<int,int>  ::iterator it;

		this->candidates.clear();
		valid_dimensions.clear();
		/*
		CosinResolver *csr = new CosinResolver(data);
		if (!csr->candidates.empty())
		{//sa jacys kandydaci
		}
		*/
		u= vector;//data->vectorOfInterest;
		int i=0;
		double E=epsilon;
		double wartosc=0.0;
		int dlugosc_i=0;
		std::vector <int> lista_nz;
		double dl_u= u->getLength();
		double wzor= (1-E*E) * (u->getLength() * u->getLength());

		sets= u->getSetOfValues();
		double war=0;

		/* poprawka */
		it=sets.begin(); //it!=sets.end(); it++){
			war= (it->second) *(it->second);
			 valid_dimensions.push_back( it->first);
			while (war<=wzor) 
			 {		it++;
				 valid_dimensions.push_back( it->first); //ten wymiar wystarczy
				 war+= (it->second) *(it->second);
			 } ;//while ((war+= (it->second) *(it->second) ) <=  wzor);

		/* koniec poprawki wydajnosci */

		// }

		//	 valid_dimensions- jak w tych wymiarach cos jest to to nasz kandydat

		bool not_added=true;
		std::vector< std::vector<Vector *>> &iv= data->invInd;
		std::vector< Vector *> result;
		std::vector< Vector *> ::iterator can;
		for ( int k=0; k<valid_dimensions.size();k++){
			//if(k%100 == 0) std::cout<< k <<"/" << valid_dimensions.size() << " processed, still processing\n";
			for ( vd= iv[valid_dimensions[k]].begin(); vd!=iv[valid_dimensions[k]].end();vd++){

				if (candidates.empty())
					candidates.push_back(*vd);
				else  {
					not_added =true;
					for (can=candidates.begin();can!=candidates.end();can++)
					{
						if (*can==*vd) { not_added=false; break; }

					}
					if (not_added) candidates.push_back(*vd);

				}


			}

		}



		/*
		//lista_nz = u->getNZD(); //na vector<int> ?  //NIE trzeba bo i tak zerowych nie trzymamy i po prostu w setofvalues ich nie ma



		*/
		/*lub jesli j to niezerowe u x zerowe z data[i] 
		przejechac liste inverted i moze do vector<int> zapisac sobie wektory wszystkie ktore sa w tych samych rzedach- maja te same wymiary niepuste co u
		uwaga by sie nie powtarzaly, i nie dodal u niechcacy

		potem dla kazdego znalezionego wektora  
		data->setOfVectors[i]->getZD();

		//znalezc podzbior tych z NZD z a i ZD z i jednoczesnie
		zrobic ai z a w ktorym tylko ten pozdzbior wymiarow zostanie

		if ( cos(a, i) >=E )

		else //wektor i nie jest kandydatem, i++
		;

		*/ 
		

		return candidates; //TODO fill candidates
	}

	std::vector <int> getJSet(){
		return valid_dimensions;
	}

};
#endif